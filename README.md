﻿<div align="center">
<br>
<h1>GimViewer </h1>
<br>
一款Unity3D高效Gim模型解析工具！可对Gim基本图元，IFC建筑模型，STL模型，高效解析<br><br>
</div>
> 使用必须遵守国家法律法规，⛔不允许非法项目使用，后果自负❗

🚩特色功能

| 支持功能 | 功能描述                                                                    | 完成程度 |
|------|-------------------------------------------------------------------------|------|
| 基本图元 | 几何图形构建 | ✅    |
| IFC | 解析IFC格式模型                | ✅    |
| STL   | 解析STL格式模型                                       | ✅    |
| 部件属性   | 部件参数详细介绍                                                        | ✅    |
| 树形层级   | 树形结构展示模型层级                        | ✅    |
| 场景环境   | 可对场景模拟真实天气环境                       | ✅    |

# 解析效果
**视频效果：https://www.bilibili.com/video/BV1oB4y1Z7yJ/**
## 白天
![在这里插入图片描述](doc/image/2112e64ecdd7b25fe11461fd38e3372.png)
## 夜晚
![在这里插入图片描述](doc/image/e6dc66d8364e9f4fc4962dddafe3c40.png)![在这里插入图片描述](doc/image/9c87a8b767bde17e5d9cc9a68cdfd90.png)

## 暴雨
![在这里插入图片描述](doc/image/74d21f3181d54be834ff6f2c1bf72c9.png)
## 暴雪
![在这里插入图片描述](doc/image/a2d3af7ca0e638fb7e2f679330d1ba5.png)
## 打雷
![在这里插入图片描述](doc/image/f0da8c82ab0cd0dba1e9c6d691fda27.png)




## 🍓依赖版本

| 依赖                         | 版本         |
|----------------------------|------------|
| ProBuilder Mesh                | 5.0.7     |
| STL                | 1.0.0  |
| Xbim       |5.1 |
|Unistorm|-|
# 使用教程
1. 使用Git工具下载或者克隆到本地并使用Unity软件打开
```git clone https://gitee.com/cangyundashuju/gimviewer.git ```

![在这里插入图片描述](doc/image/1696834527607.jpg)

2. 运行点击打开按钮选择需要解析的Gim模型文件（.gim）或doc目录下测试模型 byq.gim进行解析

![在这里插入图片描述](doc/image/1696834546354.jpg)

# 贡献力量

- 大家可以贡献代码维护开源项目

# 其它说明

- 商业合作 893500765@qq.com

